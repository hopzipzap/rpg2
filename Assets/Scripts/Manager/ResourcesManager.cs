﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG2.Manager {
    public class ResourcesManager : MonoBehaviour {
        public Inventory.Inventory inventory;
        void Awake () {
            inventory.Initialize ();
        }

        public Inventory.Items GetItem (string id) {
            return inventory.GetItem (id);
        }

        public Inventory.Weapon GetWeapon (string id) {
            Inventory.Items item = GetItem (id);
            return (Inventory.Weapon) item.obj;
        }
    }
}
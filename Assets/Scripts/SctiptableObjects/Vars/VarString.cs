﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace SA.ScriptableObjects {
    [CreateAssetMenu(menuName = "Var/String")]
    public class VarString : ScriptableObject {
        public string strValue;
    }
}
﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace RPG2.Inventory {
    [CreateAssetMenu (menuName = "Single Instances/Inventory")]
    public class Inventory : ScriptableObject {
        public Items[] items;
        Dictionary<string, int> dict = new Dictionary<string, int> ();

        public void Initialize () {
            for (int i = 0; i < items.Length; i++) {
                if (dict.ContainsKey (items[i].id)) {

                } else {
                    dict.Add (items[i].id, i);
                }
            }
        }

        public Items GetItem (string id) {
            Items ret = null;
            int index = -1;
            if (dict.TryGetValue (id, out index)) {
                ret = items[index];
            }

            if (index == -1)
                Debug.Log ("No item found" + id);
            return ret;
        }
    }
}